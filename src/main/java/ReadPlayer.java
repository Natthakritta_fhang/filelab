
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Natthakritta
 */
public class ReadPlayer {

    public static void main(String[] args) {
        Player O = null;
        Player X = null;
        FileInputStream fis = null;
        try {
            File file = new File("player.dat");
            fis = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fis);

            O = (Player) ois.readObject();
            X = (Player) ois.readObject();

            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
           O = new Player('O');
           X = new Player('X');
        } catch (IOException ex) {
            Logger.getLogger(ReadFriend.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ReadPlayer.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(ReadFriend.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        System.out.println(O);
        System.out.println(X);
    }
}
