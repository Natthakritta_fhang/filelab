
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Natthakritta
 */
public class WritePlayer {

    public static void main(String[] args) {
        Player O = new Player('O');
        Player X = new Player('X');
        O.Win();
        X.Loss();
        O.Draw();
        X.Draw();
        O.Win();
        X.Loss();
        System.out.println(O);
        System.out.println(X);
        FileOutputStream fos = null;
        try {
            File file = new File("player.dat");
            fos = new FileOutputStream(file);
            ObjectOutputStream Oos = new ObjectOutputStream(fos);
            Oos.writeObject(O);
            Oos.writeObject(X);

            Oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(WriteFriend.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(WriteFriend.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(WriteFriend.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
