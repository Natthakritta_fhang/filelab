
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Natthakritta
 */
public class WriteFriendList {
    public static void main(String[] args) {
        FileOutputStream fos = null;
        try {
             LinkedList<Friend> friendList= new LinkedList<>();
            friendList.add(new Friend("Worawit",43,"088123456"));
            friendList.add(new Friend("Fhang",21,"0659190965"));
            friendList.add(new Friend("Nat",20,"0929598891"));
            
            File file = new File("List_of_friend.dat");
            fos = new FileOutputStream(file);
            ObjectOutputStream Oos = new ObjectOutputStream(fos);
            Oos.writeObject(friendList);
            
            Oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(WriteFriend.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(WriteFriend.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(WriteFriend.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
