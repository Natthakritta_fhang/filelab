
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Natthakritta
 */
public class ReadFriendList {

    public static void main(String[] args) {
        FileInputStream fis = null;
        try {
            LinkedList<Friend> friendList=null;
        
            File file = new File("List_of_friend.dat");
            fis = new FileInputStream(file);
            ObjectInputStream ois  =new ObjectInputStream(fis);
            friendList = (LinkedList<Friend>) ois.readObject();
            
            for (Friend friend : friendList) {
                System.out.println(friend);
            }
           
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ReadFriend.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ReadFriend.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ReadFriendList.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fis.close();
            } catch (IOException ex) {
                Logger.getLogger(ReadFriend.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
